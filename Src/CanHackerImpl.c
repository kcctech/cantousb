/*
 * CanHackerImpl.c
 *
 *  Created on: 22 ����. 2018 �.
 *      Author: kcc
 */
#include "CanHackerImpl.h"
#include "stdbool.h"

uint8_t interface_state = 0;
static volatile bool CanTransmitIsPending;

//extern void HW_CanRecieveMessages(void);
extern uint32_t HW_GetTimerValue(void);
extern uint8_t CDC_add_buf_to_transmit(uint8_t* cmdBuf, uint16_t cmdLen);

uint8_t halfbyte_to_hexascii(uint8_t _halfbyte);

extern CAN_HandleTypeDef hcan;

uint8_t hexascii_to_halfbyte(char asciiChar);

void CanHacker_SetPendingCanTransmission(void)
{
    CanTransmitIsPending = true;
}

void CanHacker_HandleCANTransmission(void)
{
    HAL_StatusTypeDef res;

    if(CanTransmitIsPending==true)
    {

        res = HAL_CAN_Transmit(&hcan, 10);

        if (res == HAL_TIMEOUT)
        {
            __HAL_CAN_TRANSMIT_STATUS(&hcan, CAN_TXMAILBOX_0);
            __HAL_CAN_TRANSMIT_STATUS(&hcan, CAN_TXMAILBOX_1);
            __HAL_CAN_TRANSMIT_STATUS(&hcan, CAN_TXMAILBOX_2);

            return;
        }

        HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_13);          /*For indicating operation outside*/
        CanTransmitIsPending = false;
    }

}


uint16_t CanHacker_ProcessCmdFromHost(char* cmdBuf, uint32_t *cmdLen, uint8_t* answBuf)
{
    int num_bytes;
    uint8_t i ;
    uint8_t tmp_byte ;

    switch(cmdBuf[0])
     {
        case 't':
         i = 1 ;

             // 3 characters of the CAN address
             hcan.pTxMsg->StdId = hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->StdId = (hcan.pTxMsg->StdId << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->StdId = (hcan.pTxMsg->StdId << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;

             hcan.pTxMsg->IDE = CAN_ID_STD;

             hcan.pTxMsg->DLC = hexascii_to_halfbyte(cmdBuf[i++]) ;

         tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (uint8_t)(tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[0] = tmp_byte ;

         tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[1] = tmp_byte ;

         tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[2] = tmp_byte ;
         tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[3] = tmp_byte ;
         tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[4] = tmp_byte ;
         tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[5] = tmp_byte ;
         tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[6] = tmp_byte ;
         tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[7] = tmp_byte ;

             CanHacker_SetPendingCanTransmission();

         num_bytes = sprintf((char*)answBuf,"\r");
         break ;

        case 'T':
         i = 1 ;

            // 8 characters of the CAN address
             hcan.pTxMsg->ExtId = hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->ExtId = (hcan.pTxMsg->ExtId << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->ExtId = (hcan.pTxMsg->ExtId << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->ExtId = (hcan.pTxMsg->ExtId << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;

             hcan.pTxMsg->ExtId = (hcan.pTxMsg->ExtId << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->ExtId = (hcan.pTxMsg->ExtId << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->ExtId = (hcan.pTxMsg->ExtId << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->ExtId = (hcan.pTxMsg->ExtId << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;

             hcan.pTxMsg->IDE = CAN_ID_EXT;

             hcan.pTxMsg->DLC = hexascii_to_halfbyte(cmdBuf[i++]) ;


         tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[0] = tmp_byte ;


         tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[1] = tmp_byte ;

          tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[2] = tmp_byte ;
         tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[3] = tmp_byte ;
         tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[4] = tmp_byte ;
         tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[5] = tmp_byte ;
         tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[6] = tmp_byte ;
         tmp_byte = hexascii_to_halfbyte(cmdBuf[i++]) ; tmp_byte = (tmp_byte << 4) + hexascii_to_halfbyte(cmdBuf[i++]) ;
             hcan.pTxMsg->Data[7] = tmp_byte ;

             CanHacker_SetPendingCanTransmission();

         num_bytes = sprintf((char*)answBuf,"\r");
         break ;

        case 'V':
            num_bytes = sprintf((char*)answBuf,"V0109\r");
        break ;

        case 'v':
            num_bytes = sprintf((char*)answBuf,"vSTM32\r");
        break ;

        case 'O':
            interface_state = 1 ;
                num_bytes = sprintf((char*)answBuf,"\r");
        break ;

        case 'C':
            interface_state = 0 ;
            //  LedR(0);LedG(0);LedB(0);
                num_bytes = sprintf((char*)answBuf,"\r");
        break ;

        case 'N':       //�������� �����, ������� ��� ������ ���� , �������� ����� � �������� ���������� ������
                num_bytes = sprintf((char*)answBuf,"ast!\r");
        break ;

        case 'S':       //�������� �������� CAN, ������� ��� ������ ���� , �������� ����� � �������� ���������� ������
                                //��� ������� ��������� ���������� CAN ��� ��������� ������������ ���������.
                                //��� �������� ������ ����� ��������� ������� ��� ���� ���������� ��������� � ������ ������ ����� ������� "C".
//              if(Buf[1]==0x35) {LedR(1);LedB(0);}
//              if(Buf[1]==0x36) {LedR(0);LedB(1);}
                switch(cmdBuf[1])
                    {

                        case    '3':                            //CAN = 100Kbls
                            MX_CAN1_100();
                            //LedR(1);LedG(1);LedB(0);
                        break ;

                        case    '4':                            //CAN = 125Kbls
                            MX_CAN1_125();
                            //LedR(1);LedG(0);LedB(0);
                        break ;

                        case    '5':                            //CAN = 250Kbls
                            MX_CAN1_250();
                            //LedR(0);LedG(1);LedB(0);
                        break ;

                        case    '6':                            //CAN = 500Kbls
                            MX_CAN1_500();
                            //LedR(0);LedG(0);LedB(1);
                        break ;

                        case    '7':                            //CAN = 800Kbls
                            MX_CAN1_800();
                            //LedR(0);LedG(1);LedB(1);
                        break ;

                        case    '8':                            //CAN = 1000Kbls
                            MX_CAN1_1000();
                            //LedR(1);LedG(0);LedB(1);
                        break ;

                        case    '9':                            //CAN = 95.238Kbls
                            MX_CAN1_95();
                            //LedR(1);LedG(0);LedB(1);
                        break ;

                        case    'a':                            //CAN = 83.338Kbls
                            MX_CAN1_83();
                            //LedR(1);LedG(1);LedB(1);
                        break ;


                        default :
                            MX_CAN1_500();
                            //LedR(1);LedG(1);LedB(0);
                        break ;

                        break ;
                    }

                num_bytes = sprintf((char*)answBuf,"\r");

        break ;

        default :
            num_bytes = sprintf((char*)answBuf,"\r");
        break ;
     }
    return (uint16_t)num_bytes;
}


void CanHacker_CAN_RxCallback(CAN_HandleTypeDef* CanHandle)
{
    uint32_t num_bytes ;
    uint8_t buf[200] ;
    static uint32_t time ;
    uint32_t CanID;
    time = HW_GetTimerValue();
    num_bytes = 0 ;

    if (CanHandle->pRxMsg->IDE == CAN_ID_STD)
    { /*Processing packet with Std addr*/
        CanID = CanHandle->pRxMsg->StdId;
        buf[num_bytes++] = 't';
        buf[num_bytes++] = halfbyte_to_hexascii((uint8_t) ((CanID >>  8) & 0x0F));
        buf[num_bytes++] = halfbyte_to_hexascii((uint8_t) ((CanID >>  4) & 0x0F));
        buf[num_bytes++] = halfbyte_to_hexascii((uint8_t) ((CanID) & 0x0F));
    }
    else
    { /*Processing packet with Ext addr*/
        CanID = CanHandle->pRxMsg->ExtId;
        buf[num_bytes++] = 'T';
        buf[num_bytes++] = halfbyte_to_hexascii((uint8_t) ((CanID >> 28) & 0x0F));
        buf[num_bytes++] = halfbyte_to_hexascii((uint8_t) ((CanID >> 24) & 0x0F));
        buf[num_bytes++] = halfbyte_to_hexascii((uint8_t) ((CanID >> 20) & 0x0F));

        buf[num_bytes++] = halfbyte_to_hexascii((uint8_t) ((CanID >> 16) & 0x0F));
        buf[num_bytes++] = halfbyte_to_hexascii((uint8_t) ((CanID >> 12) & 0x0F));
        buf[num_bytes++] = halfbyte_to_hexascii((uint8_t) ((CanID >>  8) & 0x0F));
        buf[num_bytes++] = halfbyte_to_hexascii((uint8_t) ((CanID >>  4) & 0x0F));
        buf[num_bytes++] = halfbyte_to_hexascii((uint8_t) ((CanID) & 0x0F));
    }
    buf[num_bytes++] = halfbyte_to_hexascii((uint8_t)(CanHandle->pRxMsg->DLC));

    // Copy data from CAN interface and transform it into text string depending on length
    for(uint8_t index = 0; index < CanHandle->pRxMsg->DLC; index++)
    {
        buf[num_bytes++] = halfbyte_to_hexascii((CanHandle->pRxMsg->Data[index])>>4);
        buf[num_bytes++] = halfbyte_to_hexascii((CanHandle->pRxMsg->Data[index]));
    }

    buf[num_bytes++] = halfbyte_to_hexascii((uint8_t)(time>>12));
    buf[num_bytes++] = halfbyte_to_hexascii((uint8_t)(time>>8));
    buf[num_bytes++] = halfbyte_to_hexascii((uint8_t)(time>>4));
    buf[num_bytes++] = halfbyte_to_hexascii((uint8_t)(time>>0));
    buf[num_bytes++] = '\r';

    if(interface_state == 1) CDC_add_buf_to_transmit(buf,num_bytes);    //���� ������� ����� - �� ���������� � ����
}


uint8_t halfbyte_to_hexascii(uint8_t _halfbyte)
{
 _halfbyte &= 0x0F ;
 if(_halfbyte >= 0x0A)
     {
         return (uint8_t)('A' + _halfbyte - 0x0A);
     }
    else
    {
        return(uint8_t)('0' + _halfbyte);
    }
}



uint8_t hexascii_to_halfbyte(char asciiChar)
{
    uint8_t result;
    do
    {
        if ((asciiChar >= '0') && (asciiChar <= (uint8_t)'9'))
        {
            result = (uint8_t)(asciiChar - (uint8_t)'0');
            break;
        }

        if ((asciiChar >= (uint8_t)'A') && (asciiChar <= (uint8_t)'F'))
        {
            result = (uint8_t)(asciiChar - 'A' + 0x0A);
        }

        if ((asciiChar >= (uint8_t)'a') && (asciiChar <= (uint8_t)'f'))
        {
            result = (uint8_t)(asciiChar - 'a' + 0x0A);
            break;
        }
    }
    while(false);

    return result;
}


 void MX_CAN1_1000(void)
 {
   hcan.Instance = CAN;
   hcan.Init.Prescaler = 3;
   hcan.Init.Mode = CAN_MODE_NORMAL;
   hcan.Init.SJW = CAN_SJW_1TQ;
   hcan.Init.BS1 = CAN_BS1_6TQ;
   hcan.Init.BS2 = CAN_BS2_5TQ;
   hcan.Init.TTCM = DISABLE;
   hcan.Init.ABOM = DISABLE;
   hcan.Init.AWUM = DISABLE;
   hcan.Init.NART = DISABLE;
   hcan.Init.RFLM = DISABLE;
   hcan.Init.TXFP = DISABLE;
   HAL_CAN_Init(&hcan);
 }
 //-------------------------------------------------------------------------------------------------------
 /* CAN1 init function   (800Kbps) */
 void MX_CAN1_800(void)
 {
   hcan.Instance = CAN;
   hcan.Init.Prescaler = 3;
   hcan.Init.Mode = CAN_MODE_NORMAL;
   hcan.Init.SJW = CAN_SJW_1TQ;
   hcan.Init.BS1 = CAN_BS1_6TQ;
   hcan.Init.BS2 = CAN_BS2_8TQ;
   hcan.Init.TTCM = DISABLE;
   hcan.Init.ABOM = DISABLE;
   hcan.Init.AWUM = DISABLE;
   hcan.Init.NART = DISABLE;
   hcan.Init.RFLM = DISABLE;
   hcan.Init.TXFP = DISABLE;
   HAL_CAN_Init(&hcan);
 }
 //-------------------------------------------------------------------------------------------------------
 /* CAN1 init function   (500Kbps) */
 void MX_CAN1_500(void)
 {
   hcan.Instance = CAN;
   hcan.Init.Prescaler = 4;
   hcan.Init.Mode = CAN_MODE_NORMAL;
   hcan.Init.SJW = CAN_SJW_1TQ;
   hcan.Init.BS1 = CAN_BS1_12TQ;
   hcan.Init.BS2 = CAN_BS2_5TQ;
   hcan.Init.TTCM = DISABLE;
   hcan.Init.ABOM = DISABLE;
   hcan.Init.AWUM = DISABLE;
   hcan.Init.NART = DISABLE;
   hcan.Init.RFLM = DISABLE;
   hcan.Init.TXFP = DISABLE;
   HAL_CAN_Init(&hcan);
 }
 //-------------------------------------------------------------------------------------------------------
 /* CAN1 init function   (250Kbps) */
 void MX_CAN1_250(void)
 {
   hcan.Instance = CAN;
   hcan.Init.Prescaler = 12;
   hcan.Init.Mode = CAN_MODE_NORMAL;
   hcan.Init.SJW = CAN_SJW_1TQ;
   hcan.Init.BS1 = CAN_BS1_6TQ;
   hcan.Init.BS2 = CAN_BS2_5TQ;
   hcan.Init.TTCM = DISABLE;
   hcan.Init.ABOM = DISABLE;
   hcan.Init.AWUM = DISABLE;
   hcan.Init.NART = DISABLE;
   hcan.Init.RFLM = DISABLE;
   hcan.Init.TXFP = DISABLE;
   HAL_CAN_Init(&hcan);
 }
 //-------------------------------------------------------------------------------------------------------
 /* CAN1 init function   (125Kbps) */
 void MX_CAN1_125(void)
 {
   hcan.Instance = CAN;
   hcan.Init.Prescaler = 24;
   hcan.Init.Mode = CAN_MODE_NORMAL;
   hcan.Init.SJW = CAN_SJW_1TQ;
   hcan.Init.BS1 = CAN_BS1_6TQ;
   hcan.Init.BS2 = CAN_BS2_5TQ;
   hcan.Init.TTCM = DISABLE;
   hcan.Init.ABOM = DISABLE;
   hcan.Init.AWUM = DISABLE;
   hcan.Init.NART = DISABLE;
   hcan.Init.RFLM = DISABLE;
   hcan.Init.TXFP = DISABLE;
   HAL_CAN_Init(&hcan);
 }
 //-------------------------------------------------------------------------------------------------------
 /* CAN1 init function   (100Kbps) */
 void MX_CAN1_100(void)
 {
   hcan.Instance = CAN;
   hcan.Init.Prescaler = 30;
   hcan.Init.Mode = CAN_MODE_NORMAL;
   hcan.Init.SJW = CAN_SJW_1TQ;
   hcan.Init.BS1 = CAN_BS1_6TQ;
   hcan.Init.BS2 = CAN_BS2_5TQ;
   hcan.Init.TTCM = DISABLE;
   hcan.Init.ABOM = DISABLE;
   hcan.Init.AWUM = DISABLE;
   hcan.Init.NART = DISABLE;
   hcan.Init.RFLM = DISABLE;
   hcan.Init.TXFP = DISABLE;
   HAL_CAN_Init(&hcan);
 }
 //-------------------------------------------------------------------------------------------------------
 /* CAN1 init function   (95.238Kbps) */
 void MX_CAN1_95(void)
 {
   hcan.Instance = CAN;
   hcan.Init.Prescaler = 27;
   hcan.Init.Mode = CAN_MODE_NORMAL;
   hcan.Init.SJW = CAN_SJW_1TQ;
   hcan.Init.BS1 = CAN_BS1_6TQ;
   hcan.Init.BS2 = CAN_BS2_7TQ;
   hcan.Init.TTCM = DISABLE;
   hcan.Init.ABOM = DISABLE;
   hcan.Init.AWUM = DISABLE;
   hcan.Init.NART = DISABLE;
   hcan.Init.RFLM = DISABLE;
   hcan.Init.TXFP = DISABLE;
   HAL_CAN_Init(&hcan);
 }
 //-------------------------------------------------------------------------------------------------------
 /* CAN1 init function   (83.33Kbps) */
 void MX_CAN1_83(void)
 {
   hcan.Instance = CAN;
   hcan.Init.Prescaler = 36;
   hcan.Init.Mode = CAN_MODE_NORMAL;
   hcan.Init.SJW = CAN_SJW_1TQ;
   hcan.Init.BS1 = CAN_BS1_5TQ;
   hcan.Init.BS2 = CAN_BS2_6TQ;
   hcan.Init.TTCM = DISABLE;
   hcan.Init.ABOM = DISABLE;
   hcan.Init.AWUM = DISABLE;
   hcan.Init.NART = DISABLE;
   hcan.Init.RFLM = DISABLE;
   hcan.Init.TXFP = DISABLE;
   HAL_CAN_Init(&hcan);
 }

