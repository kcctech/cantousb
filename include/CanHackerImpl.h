#pragma once

#include "stm32f3xx_hal.h"

 void CanHacker_CAN_RxCallback(CAN_HandleTypeDef* CanHandle);
 void CanHacker_SetPendingCanTransmission(void);
 void CanHacker_HandleCANTransmission(void);
 uint16_t CanHacker_FillUsbTransmitBuffer(char* cmdBuf, uint32_t *cmdLen, uint8_t* answBuf);
 void MX_CAN1_1000(void);
 void MX_CAN1_800(void);
 void MX_CAN1_500(void);
 void MX_CAN1_250(void);
 void MX_CAN1_250(void);
 void MX_CAN1_125(void);
 void MX_CAN1_100(void);
 void MX_CAN1_95(void);
 void MX_CAN1_83(void);



